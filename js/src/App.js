import React, { useState, useEffect } from 'react';
import logo from './logo.svg';
import './App.css';

const LOADING_STATE_EMPTY = 0;
const LOADING_STATE_LOADING = 1;
const LOADING_STATE_LOADED = 2;

const fetchASIN = async (asin) => {
  return fetch(`/api/product/${asin}`)
}

function App() {
  const [asin, setASIN] = useState(window.location.pathname.replace(/^\//, '') || null);
  const [productInfo, setProductInfo] = useState('');
  const [loadingState, setLoadingState] = useState(LOADING_STATE_EMPTY);

  useEffect(() => {
    // debugger;
    if (asin) {
      // Change URL first
      window.history.pushState({}, '', `/${asin}`);
      // To prevent this from showing up when the db has it cached
      const loadingTimeout = setTimeout(() => setLoadingState(LOADING_STATE_LOADING), 200);
      fetchASIN(asin)
        .then(response => {
          return response.text();
        }).then(text => {
          if (loadingTimeout) clearTimeout(loadingTimeout);
          setLoadingState(LOADING_STATE_LOADED);
          setProductInfo(text);
        });
    } else {
      window.history.pushState({}, '', '/');
      setLoadingState(LOADING_STATE_EMPTY);
    }
  }, [asin, ])
  
  const changeASIN = e => {
    e.preventDefault();
    setASIN(e.currentTarget.children[1].value);
  };

  return (
    <div className="main">
      <form onSubmit={changeASIN}>
        <label htmlFor="asin">Please enter an <acronym title="Amazon Standard Identification Number">ASIN</acronym>, then hit enter.</label>
        <input type="text" id="asin" defaultValue={asin} placeholder="e.g. B00UWWC810" />
        <button type="submit">Go!</button>
      </form>
      {asin &&
      <div>
        <h1>Product {asin}</h1>
        {loadingState === LOADING_STATE_LOADED && <p>{productInfo}</p>}
        {loadingState === LOADING_STATE_LOADING && <p>Loading</p>}
      </div>
      }
    </div>
  );
}

export default App;
