import unittest

from scraper.scrape_asin import scrape_asin

class TestScraper(unittest.TestCase):

    def test_three_wolf_moon(self):
        """
        Test with the famous 3WM t-shirt, where the product info
        is not in a table but in a list.
        """
        file = open("./tests/test-case-three-wolf-moon-product-info-in-list-B00UWWC810.html", "r")
        html_string = file.read()
        self.assertIsNotNone(html_string)
        (category, rank, dimensions) = scrape_asin(html_string)
        self.assertEqual(category, "Clothing, Shoes & Jewelry")
        self.assertEqual(rank, 24817)
        self.assertEqual(dimensions, '4 x 8 x 1 inches')
        file.close()

    def test_banana_brush(self):
        """
        Test with the famous 3WM t-shirt, where the product info
        is not in a table but in a list.
        """
        file = open("./tests/test-case-banana-brush-table-info-B002QYW8LW.html", "r")
        html_string = file.read()
        self.assertIsNotNone(html_string)
        (category, rank, dimensions) = scrape_asin(html_string)
        self.assertEqual(category, "Baby")
        self.assertEqual(rank, 26)
        self.assertEqual(dimensions, '4.3 x 0.4 x 7.9 inches')
        file.close()

    def test_finger_hands(self):
        """
        Test with the famous 3WM t-shirt, where the product info
        is not in a table but in a list.
        """
        file = open("./tests/finger-hands-product-info-in-table-B00OACD9CU.html", "r")
        html_string = file.read()
        self.assertIsNotNone(html_string)
        (category, rank, dimensions) = scrape_asin(html_string)
        self.assertEqual(category, "Toys & Games")
        self.assertEqual(rank, 3840)
        self.assertEqual(dimensions, None)
        file.close()