# ASINATOR!

📦💳 The ASINATOR will take your Amazon Standard Identification Number, get the category, rank and dimensions, cache that info and then deliver the data to you. Enjoy.

## Requirements

* Python 3.7.1
* Node.js 12+ (I recommend using `nvm` to install it - also this probably works fine with older versions I just didn't test it)
## Set up

1. Create a Python virtual environment (see [`mkvirtualenv`](https://virtualenvwrapper.readthedocs.io/en/latest/command_ref.html)) then run `pip install -r requirements.txt`.
2. Run `python main.py`
3. Go into `js` in a different terminal session and run `yarn` to install JS dependencies
4. Run `yarn start`
5. Navigate to http://localhost:3000 and enter in an ASIN, or go to http://localhost:3000/B002QYW8LW to just get started.

## Testing

There are no frontend tests except for the main one that comes with `create-react-app` - I spent most of my time getting the scraper to be robust as Amazon has a variety of ways of showing the data and it isn't consistent.

Run the backend tests in your `virtualenv` by running `python -m unittest tests.test_scraper`. You can easily create new test cases by downloading the HTML source of Amazon pages and putting them in the test folder, then copying an existing test case and replacing the path and asserts.

## How does it work?

The **frontend** is powered by React 16 - no extra dependencies are there, unless you count build deps like webpack and babel. I used `create-react-app` to start up the app so there are a few artefacts left over. The frontend isn't tested as I didn't have time - I felt it was more important to use TDD where most of the logic was happening - on the backend. I used this opp

The **backend** is a Python [Flask](https://palletsprojects.com/p/flask/) application, with a `sqlite3` set up database for storage. You do not need to configure this, the application is quite forgiving for setup simplicity.

It uses the popular Python `requests` library to download the Amazon pages, and the HTML downloaded is sent to a unit-tested module called `scrape_asin.py` which uses regular expressions to extract the category, rank and dimensions. When this is extracted it is stored in the DB, and then served to the client. This step is also skipped if the ASIN can be found in the database (the ASIN is the primary key of the only table).

Flask then returns the data as a string. I was going to have this sent as JSON but in the end I ran out of time. It's still stored properly on the database, so this is fine - it would be a trivial change to the view which would not break anything important.

Dimensions are not always available, so sometimes this will be returned `None`.

Finally, it's important to note that Amazon's product pages are **not consistent**; sometimes the data is in a `span`, sometimes a `table`/`td`, and sometimes the words `Amazon Best Seller Rank` are replaced with `Best Seller Rank`. I imagine this is a legal requirement or something, I have no idea.

## Developer log

**Log one:** I started by trying use BeautifulSoup to parse the HTML, but it wasn't enough. Amazon's markup thwarted it and I don't have time to write anything to clean it, so regex will have to do. I've got category and rank out so far. Annoyingly because of BS4's BS, I have spent a whole hour! Oh well.

**Log two:** I've got it extracting the data and printing out as text in the API now.

**Log three:** Okay so react app created, uses fetch and hooks to get stuff from the API which is running in python. Proxying to port 5000 using react-scripts, which is handy for development. It now prints the data if you ask for it using the URL. Gonna get the form working now and then make the data more structured, then deal with styling. Got about 1.5 hrs left!

**Log four:** I was testing this thing with the banana toothbrush, so I downloaded the source and put it into a new test case. I asserted that the rank shoul dbe 23, because that's what I read on the page. But it kept spitting out 26. I looked at the source and it said 26! So between when I downloaded the source and my most recent refresh, the rank changed by 3 places. This highlights the importance of having consistent, offline test cases rather than testing with live, dynamic data! Anyway, it should work with the table format now.

**Log five:** Okay it's fairly robust now. Just need to get it storing stuff in the DB and then I'll have fulfilled requirements.

**Log six:** Okay, requirements fulfilled. I went a bit overtime... I'll explain why in my reasoning below.

## Postmortem

I went a bit overtime (by about half an hour), but I think that's acceptable - I had a big setback at the beginning with the HTML parsing.

### Challenges

#### Parsing HTML

I excited decided early on that `beautifulsoup4` would be my savior at the beginning for parsing the HTML - I could directly target elements using their IDs or other attributes. However, Amazon's pages are too messy (I think) for BS. It didn't like them. I tried cleaning up the HTML first but then realised it was too much, so I went to my old friend Regex to figure it out. I knew this would be more challenging in the longrun because I'd have to make forgiving-enough expressions to tackle the various ways that Amazon appeared to be displaying the data. Unfortuantely I wasted about 45 minutes in setting up beautifulsoup and trying to get it to consume the HTML properly.

#### User-Agent nonsense

I wanted to be a good web citizen and use a custom `User-Agent` header with my email address in it when making the requests to get the Amazon pages, but sadly Amazon got angry and eventually started blocking my requests - especially when I accidentally got into a loop because of some state on the frontend. So in the end I just copied my own browser's UA and it stopped complaining.

#### React hooks!

I'm new to hooks, but this was an interesting learning exercise. They are pretty straightforward to get up and running with but I did run into an infinite-loop situation when using `useEffect`, which essentially replaces the main component lifecycle methods which most people use. It was easy to fix though, thanks to my old pal StackOverflow.

#### Dynamic data!

When I was testing out the banana brush (http://localhost:3000/B002QYW8LW) my test screamed at me because I'd just read `23` as the rank on Amazon's live page, but my scraper was bringing out `26`. It turns out that in the few minutes between downloading the page for my tests and writing the test, the rank had changed from 23 to 26! This really shows how important it is for test data to be consistent and available offline, I think. Especially when working with live websites, never assume things will stay the same!

### Notable decisions

#### Robustness in product requests

I decided to focus on making sure pretty much any product page would work, so I picked a few and created unit tests for them before implementing their repsective use cases. As a result I realised I didn't have time to write tests on the frontend which was relatively straightforward anyway.

#### URL state

I thought it'd be a nice touch to make the URL change when you change the ASIN, and for it to understand the URL on first load if it contains an ASIN. In a realistic situation, a browser plugin would link directly to a page like this one so the ASIN would need to be stored somewhere - the URL is the obvious choice. It also made it easy for you all to try it out with examples like http://localhost:3000/B002QYW8LW and http://localhost:3000/B07L4DT1P7.

#### DB Structure

Given that I'm working with SQLite it would be easy to be really lazy and just create a bunch of huge tables, but I was careful not to allocate too much space to each column, given that ASIN is fixed length and realistically a category can only be a handful of words. The only one where I was a bit flexible was with rank - I don't know how big the rank numbers get but 4 bytes should be enough, thus I used `INTEGER`.

### Things I would want to fix with more time

#### Database

An SQLite database is appropriate for quick and dirty hacks like this but it wouldn't work for a production system. For the most extreme levels of volume, I'd want to get a cluster of DBs which are kept in sync and load balanced. The purpose of the DB here appears to be for caching, both for speed and to prevent Amazon from getting fed up with all the requests, so reliability and speed are important.

A relational database is a good choice for this but it's not necessary right now, all we're really using is the indexing and this is available with other types of database. Something like Mongo or ElasticSearch would probably end up being harder to work with for this kind of data though.

#### API

The API response (/api/B07L4DT1P7) is plain text. This is fine, but it's not easy to consume for the client so the data can't be presented in any other format. It'd be better to send it as JSON. Heck, even XML would be better! This isn't hard to do, I just had more pressing priorities. I think a RESTful API would be adequate here, too, but GraphQL would also be fine.

There's also no way to stop the user from trying to request a product that doesn't exist, or tell them that it doesn't exist when they request it. They get an IndexError from my pattern matching on the backend.

From an ops perspective, too, this is obviously not adequate! You can't just run a single Python script to serve a potentially very busy website. I would want to:

1. Run a whole load of instances of this, and load balance them - luckily you can use Flask with uWSGI, and even something as straightforward as `nginx` for balancing would be better than nothing, but it'd probably be better to containerise the server and have it running on AWS or GCP or something.
2. An extra layer of cache such as Redis for really popular requests would speed things up too
3. Serve the frontend code from a CDN, delivered to it whenever it is built on something like CircleCI

#### Frontend

Back button doesn't work even though the URL changes. I'm not actually listening for URL changes.

The frontend should have some basic tests: Making sure that given a certain response, it shows a certain result, and making sure the form being submitted causes state changes. The API responses could easily be mocked using jest.

It's also not super user friendly. Storing the history of different requests would be helpful, and it could be a little more attractive.

### The purpose of this product - what next?

I guess you'd probably want to use this to analyse competing products to get an idea of what sells better in what category and why. You could scrape the ranking page to find the 5 products on either side of the current one in rank.