import urllib.parse
import logging, sys
import sqlite3

from flask import Flask
from scraper.scrape_asin import scrape_asin
import requests

app = Flask(__name__, static_url_path="", static_folder="./js/build")

def set_up_db():
    conn = sqlite3.connect('asinator.db')

    # Set up the table if not already done...
    conn.execute("""
    CREATE TABLE IF NOT EXISTS product_cache (
        amazon_asin VARCHAR(10) PRIMARY KEY,
        category VARCHAR(128) NOT NULL,
        rank INT NOT NULL,
        dimensions VARCHAR(23)
    )
    """)

def get_product_from_database(asin):
    conn = sqlite3.connect('asinator.db')
    c = conn.cursor()
    c.execute("SELECT * FROM product_cache WHERE amazon_asin=?", (asin, ))
    result = c.fetchone()
    if result is None:
        return None
    (retrieved_asin, category, rank, dimensions) = result
    conn.close()
    return (category, rank, dimensions)

def get_product(asin):
    conn = sqlite3.connect('asinator.db')
    c = conn.cursor()
    cached_result = get_product_from_database(asin)
    if cached_result is not None:
        logging.debug("Returning {} from the database instead of requesting a fresh one".format(asin))
        return (cached_result[0], cached_result[1], cached_result[2])
    amazon_product_url = urllib.parse.urljoin("https://www.amazon.com/dp/", asin)
    headers = { 'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36'}
    print("Getting HTML from {}".format(amazon_product_url))
    r = requests.get(amazon_product_url, headers=headers)
    (category, rank, dimensions) = scrape_asin(r.text)
    query = "INSERT OR IGNORE INTO product_cache VALUES ('{}', '{}', {}, '{}')".format(asin, category, rank, dimensions)
    logging.debug("Storing {} in the database".format(asin))
    c.execute(query)
    conn.commit()
    conn.close()
    return (category, rank, dimensions)

@app.route('/', defaults={'asin': None})
@app.route('/<asin>')
def root(asin):
    """Make sure the index file is sent at root"""
    return app.send_static_file('index.html')

@app.route('/api/product/<asin>', methods=['GET'])
def product(asin):
    """
    In a rush, putting logic in here, should probs be elsewhere
    """
    (category, rank, dimensions) = get_product(asin)
    return "Category: {}, Rank: {}, Dimensions: {}".format(category, rank, dimensions)

if __name__ == "__main__":
    app.debug = True
    logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)
    set_up_db()
    app.run(host='localhost')