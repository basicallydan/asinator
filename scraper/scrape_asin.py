import logging, sys
import re

def scrape_asin(html_string):
    product_info_re = re.compile(r'Best Sellers Rank.*\s*#([\d,]+)\s+in (\w.*?)\(', re.DOTALL)
    try:
        product_info = product_info_re.findall(html_string)[0]
    except IndexError as e:
        raise Exception("Couldn't find the rank and/or category. Error: {}".format(str(e)))
    dimension_info_re = re.compile(r'Product Dimensions.*?>([\d\.]+ x [\d\.]+ x [\d\.]+ inches)<\/', re.DOTALL)
    try:
        dimension_info = dimension_info_re.findall(html_string)[0]
    except IndexError as e:
        # This is fine, we don't mind not having dimension info
        logging.info("Couldn't find the dimension info. Error: {}".format(str(e)))
        dimension_info = None
    return (product_info[1].strip(), int(product_info[0].replace(',', '')), dimension_info)